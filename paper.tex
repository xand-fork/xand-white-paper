\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
%\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{pgf-umlsd}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{threeparttable}
\usepackage[standard]{ntheorem}
\newcommand{\spc}{\;\;\;\;\;\;\;\;}
\definecolor{cmt}{rgb}{0.7, 0.7, 0.7}
\newcommand{\cmnt}[1]{{\color{cmt}\textit{#1}}}
\newcommand{\ifrz}[1]{\textit{if }(#1 \neq 1)\textit{ return }0;}
\newcommand{\foralli}{\forall_{i \in \{1..n\}}}
\newcommand{\forallj}{\forall_{j \in \{1..m\}}}
\newcommand{\forallt}{\forall_{t \in \{1..u\}}}
\newcommand{\foralle}{\forall_{e \in \{1..w\}}}
\allowdisplaybreaks
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Confidentiality for the Xand blockchain}

\author{\IEEEauthorblockN{1\textsuperscript{st} Debasish Ray Chawdhuri}
    \IEEEauthorblockA{\textit{Research and Development} \\
        \textit{Talentica Software Pvt. Ltd.}\\
        Pune, India \\
        https://orcid.org/0000-0001-8615-6054}

}

\maketitle

\begin{abstract}
    Xand is a permissioned blockchain that enables real-time settlement of institution money transfer in USD. Xand tokens are backed by USD and the real USD backing the token is kept with a regulated trustee that is easily auditable. Xand also addresses the confidentiality problem with other similar tokens like USDT. The current paper describes the design of confidentiality features in Xand that both protects the transactors from being spied on while also enabling regulators to get reasonable certainty in countering money laundering and enabling them to enforce accountability. This is essential for regulated entities to use the system for legitimate business.
\end{abstract}

\begin{IEEEkeywords}
    Xand, Confidentiality, Blockchain
\end{IEEEkeywords}

\section{Introduction} Blockchain has an enormous applicability in democratizing payments. One of blockchain's advantage is real-time settlement of payment without a single central party controlling it. There has been many payment systems backed by fiat currency with similar goals. Two major names of such coins are Tether and USDC. Both such tokens are Ethereum ERC20 tokens backed by USD held by a trust. However, there has been controversies and investigations regarding the trustworthiness of the trust in holding the value. It is desirable for the Trustee to be a regulated entity so that it is accountable.

Another problem with such systems is that they are ERC20 tokens on Ethereum. This means the transactions are not private. This coupled with the fact that Ethereum works on an account-based system (in contrast with a UTxO model), makes them even more traceable. Xand on the other hand has been made with privacy from the ground up. In this paper, we describe the cryptographic design of Xand. The Xand privacy model intends to ensure that transactions are anonymous while also ensure that adequate provisions are made for regulators to be able to hold the transactors accountable when required.

Xand is a permissioned blockchain that has coins backed by fiat currency, currently USD. The blockchain has the following parties with different sets of permissions -
\begin{enumerate}
    \item \textbf{Member:} A member is a party that is allowed to transact on the blockchain.
    \item \textbf{Validator:} A validator is a party that is responsible for validating the blocks, and hence the transactions. The validators are different from the members and they are rewarded financially for their service.
    \item \textbf{Trust:} The Trust is responsible for holding the funds that support the token. All transactions to and from the Trust are transparent in a manner so that anyone observing the chain can know exactly how much USD the Trust is supposed to have, while still protecting the identities of the other parties. This makes sure that the Trust can be audited very easily and can be made to produce a proof of funds periodically. The Trust is allowed to do some special transactions for this purpose.
\end{enumerate}

The above parties facilitate the following types of transactions which the members are allowed to make -
\begin{enumerate}
    \item \textbf{Transfer Transaction:} The transfer transaction is a transaction of tokens from one member to one or more other members.
    \item \textbf{Create Transaction:} The create transaction is a transaction between a member and the trust. The member creates a create transaction that is actually a request for the trust to issue the tokens. The trust only does so when the member pays an equal amount to the trust in USD (outside of the chain).
    \item \textbf{Redeem Transaction: } The member can use a redeem transaction to destroy some tokens and request the trust to pay the member with an equal amount of USD outside the chain.
\end{enumerate}

Other than these, the Trust is allowed to make the following transactions -
\begin{enumerate}
    \item \textbf{Create Acceptance Transaction: } This represents the acceptance of a given linked create transaction. The Trust makes this transaction only after verifying that an equal amount of USD has been deposited by the transactor in the Trustee bank account. After this transaction, the output TxOs of the linked create transaction can be used as inputs to any other transaction.
    \item \textbf{Create Rejection Transaction: } This represents the rejection of the create transaction. The Trust creates this transaction if the Trust verifies that the funds for the creation has not been deposited. After this transaction, the output TxOs of the linked create transaction are destroyed.
    \item \textbf{Redeem Acceptance Transaction: } This represents the acceptance of the redeem transaction. After this transaction has been made by the Trust, the Trust would pay an equal amount of USD to the redemption amount in the linked redeem transaction to the creator of the redeem transaction.
    \item \textbf{Redeem Rejection Transaction: } This represents the rejection of the redeem transaction. The Trust creates this transaction if there is any error related to the redeemer's bank account information or any other error. Since the key-image for the input transaction has already been disclosed, the redeem transaction itself contains an output public key that the amount should go to in case the redeem is rejected.
\end{enumerate}

The transactions made by the Trust do not require any confidentiality. Hence, in this paper, we will only concentrate on the transactions made by the members. However, relevant discussion about the other transactions will be given as required.

\section{Literature Review}

The Xand Network provides a business-to-business on-demand settlement solution that maintains a digital ledger using a blockchain. This distributed ledger tracks Xand Claims on US dollars held in trust. The Xand Network is permissioned and has important governance requirements about what entities can act as Members. Additionally Members require some level of privacy on their financial transactions. In this section we give a literature review for the techniques used in the confidentiality protocol for the Xand Network.

Our Xand confidentiality protocol is designed based on techniques pioneered by Monero, which is a standard one-dimensional distributed cryptocurrency blockchain \cite{nakamoto2019bitcoin}. Monero was initially known as CryptoNote \cite{van2013cryptonote} published under the pseudonym of Nicolas van Saberhagen. It offered receiver anonymity through the use of one-time addresses, and sender ambiguity by means of ring signatures. Since its appearance, Monero has further strengthened its privacy aspects by implementing amount hiding, as described by Greg Maxwell in \cite{maxwell2015borromean}, integrating ring signatures based on Shen Noether's recommendations in \cite{noether2016ring}, and made more efficient with Bulletproofs \cite{bunz2018bulletproofs}. Afterwards, there are several version updates called \textit{Zero to Monero} that keep improving cryptographic primitives used \cite{alonso2018monero,alonso2020zero}.

The Xand Confidentiality Protocol has leveraged many of the advantages of Monero while adding to the protocol with our governance requirements. Transactions in our protocol are based on elliptic curve cryptography using curve Ed25519\cite{bernstein2008twisted} and transaction inputs are signed with Schnorr-style ring signature\cite{noether2016ring}, and output amounts are communicated to recipients via Elliptic Curve Diffie-Hellman(ECDH)\cite{diffie1976new} and concealed with Pedersen commitments \cite{maxwell2015confidential} and proven in a legitimate range with Bulletproofs \cite{bunz2018bulletproofs}. We use a special curve belonging to the category of so-called \textit{Twisted Edwards} curves \cite{bernstein2008twisted}. In particular, it uses Curve25519 which was released in \cite{bernstein2008twisted}. The advantage this special curve offers is that basic cryptographic primitives require fewer arithmetic operations, resulting in faster cryptographic algorithms. More details are described in Bernstein et al.\cite{bernstein2007faster}.

A zero-knowledge proof system is a key part of our confidentiality protocol. The notion of a zero-knowledge proof system was introduced by Goldwasser, Micali and Rackoff \cite{goldwasser1989knowledge} which is central in cryptography. Since its introduction, the original definition has been then reformulated under several variations, trying to capture additional security and efficiency guarantees. In recent years, the concept of a zero knowledge proof has been extremely influential and useful for many other notions and applications in blockchain-based financial systems. Some notable examples are ZCash \cite{hopwood2016zcash}, Hyperledger Fabric \cite{androulaki2018hyperledger}, Ethereum \cite{wood2014ethereum} and Cardano's Ouroboros \cite{kerber2019ouroboros}.

We have designed our own zero-knowledge proof protocol called \textit{Zero-Knowledge Proof for Linear Member Tuple (ZKPLMT)}, which is a more generalized version to support proving a member's identity in a set of members. The protocol is based on the Schnorr Identification Protocol \cite{schnorr1989efficient, maurer2009unifying}. Identification, also known as entity authentication, is a process by which a verifier gains assurance that the identity of a prover is as claimed, i.e., there is no impersonation. An identification scheme enables a prover holding a secret key to identify itself to a verifier holding the corresponding public key. The origin Schnorr identification protocol is interactive and not publicly verifiable, we use Fiat-Shamir transform \cite{fiat1986prove}. That is, we assume the hash function as a random oracle to generate challenges in the interactive proof protocol. Our ZKPLMT protocol follows the paradigm of Schnorr Identification Protocol to construct non-interactive zero-knowledge proofs of knowledge. These are protocols which allow a prover to demonstrate knowledge of a secret while revealing no information whatsoever other than the one bit of information regarding the possession of the secret \cite{feige1988zero, goldwasser1989knowledge}. In our use-cases, we particularly focus on its application that it allows someone to prove they know the private key \(k\) of a given public key \(K\) without revealing any information about it \cite{maxwell2015borromean}.

\section{Prelimaries}
\subsection{Notation}
\begin{itemize}
    \item The left arrow ($\leftarrow$) signifies either assignment or computation or random selection. The notation $X \leftarrow Y$ means assign the value of $Y$ to the variable $X$ when $Y$ is an expression. If $\mathcal{S}$ is a set, $X \leftarrow \mathcal{S}$ represents choosing $X$ uniformly over $\mathcal{S}$. If $\mathcal{A}$ is an algorithm, $X \leftarrow \mathcal{A}$ represents running $\mathcal{A}$ once and assigning the result to $X$.
    \item We use $i$ as the index of a component of a tuple, $j$ to index a tuple in a set.
    \item We will use the operator $\forall$ to signify a sequence indexed by a variable. For example, $\forall_j (X_j, Y_j)$ represents $(X_1,Y_1), (X_2,Y_2), ...$ for all values of $j$. $\forall_{j=1}^m (c_j,d_j)$ represents $(c_1,d_1),(c_2,d_2),...,(c_m,d_m)$. Similarly, for a set $\mathcal{S}$, $\forall_{j \in \mathcal{S}} X_j$ represents a sequence of $X_j$ for all values of $j \in \mathcal{S}$.
          Notice that we use subscript and superscript with $\forall$ for this custom notation. When used with regular text position, $\forall$ has the usual meaning.
    \item $\mathbb{G}$ is an elliptic curve group of prime order $q$. We assume that $q$ is close to $2^{\lambda}$ where $\lambda$ is the security parameter.
    \item $G$ is a system-wide fixed generator of $\mathbb{G}$.
    \item $L$ is a system-wide fixed generator of $\mathbb{G}$ such that the discrete logarithm of $G$ with respect to $L$ is not known.
    \item $\mathbb{F}_q$ is the field integer modulo $q$.
    \item $\mathbb{F}^*_q$ is $\mathbb{F}_q \setminus \{0\}$.
    \item $H_g$ is a hash function with output in the group.
    \item $H_q$ is a hash function with output in $\mathbb{F}_q^*$.
    \item $H_b$ is a hash function with output in $\{0,1\}^b$ for a fixed $b$.
    \item $\phi$ is an empty string of bits or an empty message.
    \item Bold $\mathbf{P}$ is the prover algorithm for ZkPLMT.
    \item Bold $\mathbf{V}$ is the verification algorithm for ZkPLMT.
\end{itemize}

\subsection{Definitions}

\begin{definition}{Negligible function}
    A function $f:\mathbb{N} \rightarrow \mathbb{R}^+$ is negligible in $\lambda$ if for every polynomical $\mathbf{p}$, there exists $\mathbf{n}$ such that $f(\lambda) < \frac{1}{\mathbf{p}}$ whenever $\lambda \ge \mathbf{n}$.
\end{definition}

\begin{definition}{DDH Assumption} We assume the DDH problem is hard to solve in the group $\mathbb{G}$. The advantage of a DDH adversary $\mathcal{C}$ is given as -

    $Adv^{DDH}(\mathcal{C}) =$

    \[\Biggl\lvert
        Pr\left[
            \begin{array}{ll}
                \rho=1
            \end{array} \left|
            \begin{array}{ll}
                G \leftarrow \mathbb{G}  ;       \\
                (x,y) \leftarrow \mathbb{F}_q^2; \\
                \rho =                           \\
                \spc \mathcal{C}(G,xG,yG,xyG);
            \end{array}
            \right.
            \right]\\
    \]
    \[
        -
        Pr\left[
            \begin{array}{ll}
                \rho=1
            \end{array} \left|
            \begin{array}{ll}
                G \leftarrow \mathbb{G}  ;       \\
                (x,y) \leftarrow \mathbb{F}_q^2; \\
                R \leftarrow \mathbb{G};         \\
                \rho =                           \\
                \spc \mathcal{C}(G,xG,yG,R);
            \end{array}
            \right.
            \right]\Biggl\rvert\\
    \]
\end{definition}
We assume that the advantage of any PPTA $\mathcal{C}$ is negligible in the security parameter $\lambda$.
\input{formal_requirements.tex}
\section{Performance}
\section{Summary} The Xand blockchain is a permissioned chain that is primarily meant to be used by money-transfer facilitators for flawless real-time settlement. The cryptography is designed to provide confidentiality to the members about their identities and the transaction amounts while also providing auditability and enabling Government oversight. The Government cannot discretely follow transaction patterns, however, with a court order, the Government can require the members to disclose the details of transactions. This provides the required legitimacy to the entire system.

\bibliographystyle{./bibliography/IEEEtran}
\bibliography{./bibliography/paper}


\end{document}
