The page is always deployed to the following location - 
https://transparentincdevelopment.gitlab.io/xand-white-paper/paper.pdf

To build using native latex installation
latexmk -pdf

It is setup to handle any other latex tool

Licensed under MIT OR Apache-2.0